package com.sda.fundamentals;

import java.util.Scanner;

public class Exercise {
    public static void getSumOFDigits() {
        int positiveNumber;
        Scanner scanner = new Scanner(System.in);
        positiveNumber = scanner.nextInt();

        if (positiveNumber < 0) {
            System.out.println("The number should be pozitive ");
            return;

        }
        int sum = 0;
        while (positiveNumber > 0) {
            int digit = positiveNumber % 10; //pt a afla ultima cifra
            sum += digit;
            positiveNumber /= 10;

        }
        System.out.println("sum of digits is = " + sum);
    }

    public static void textreader() {

        Scanner scanner = new Scanner(System.in);
        String[] texts = new String[100];
        int index = 0;

        while (true) {
            String text = scanner.nextLine();
            if (text.equals("Enough!")) {
                break;

            }
            texts[index] = text;
            index++;

        }
        String longestTexts = texts[0];
        for (int i = 0; i < index; i++) {

            if (longestTexts.length() < texts[i].length()) {
                longestTexts = texts[i];
            }
        }
        System.out.println("The longest text is:" + longestTexts);
    }

    public static void solutionWithVariables() {
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        String longestText = text;
        while (!text.equals("Enough!")) {
            text = scanner.nextLine();
            if (text.length() > longestText.length()) {
                longestText = text;

            }
        }
        if (longestText.equals("Enough!")) {
            System.out.println("now text provaided");

        }

    }

    public static void gapBetweenLetters() {
        Scanner scanner = new Scanner(System.in);
        char letter1 = scanner.next().charAt(0);
        char letter2 = scanner.next().charAt(0);
        int asciRepresentation = (int) letter1;
        int asciRepresentation1 = (int) letter1;
        int asciiRepresentation2 = (int) letter2;
        int difference = Math.abs(asciRepresentation1 - asciiRepresentation2);
        if (difference == 0) {
            System.out.println("Letters are identical");
        } else {
            System.out.println();
        }


        System.out.println("there are" + (difference - 1) + "characteres between the letters ");


    }

    public static void longetSubsequents() {
        int[] subsequents = new int[10];
        Scanner numbers = new Scanner(System.in);


        for (int i = 0; i < subsequents.length; i++) {
            int number = numbers.nextInt();
            subsequents[i] = number;
            System.out.println(subsequents[i]);

            //iterem cu for pt a citi de la tastatura 10 numere
        }
        int sum = 1;
        int longestSubsequents = 1;
        for (int j = 1; j < subsequents.length; j++) {
            if (subsequents[j] > subsequents[j - 1]) {
                //in variabila sum vom retine lungimea susecventeri curente
                sum++;

            } else {
                longestSubsequents = Math.max(sum, longestSubsequents);
                sum = 1;
            }
            System.out.println(longestSubsequents);

        }

    }
}
