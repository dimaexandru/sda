package com.sda.fundamentals;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Exercise.getSumOFDigits();
        boolean found = false;



        do {

            Scanner scanner = new Scanner(System.in);
            float firstNumber = scanner.nextFloat();
            String operator = scanner.next().trim();
            float secondNumber = scanner.nextFloat();

            String[] symbolsList = new String[]{"+", "-", "/", "*"};

            for (int i = 0; i < symbolsList.length; i++) {
                // folosim equals pentru a compara valoarea obiectului de tipul String.Daca folosim "==", atunci se vor compara adresele de memorie(care pot fi diferite)
                if (operator.equals(symbolsList[i])) {
                    found = true;
                }
            }
            if (found == false) {
                System.out.println("Cannot calculate");
            } else {


                Calculator calculator = new Calculator();
                calculator.calculate(firstNumber, operator, secondNumber);
                float result = calculator.calculate(firstNumber, operator, secondNumber);
                System.out.println(result);
            }

        } while (found == false);
    }


             }