package com.sda.fundamentals.poemtask;

public class Main {
    public static void main(String[] args) {
        Author author1 = new Author("Michael","English");
        Author author2 = new Author("Ioan","Romania");
        Poem poem1 =new Poem(author1,5) ;
        Poem poem2 =new Poem(author2,7);
        Poem poem3 =new Poem (author2,7);

        Poem [] poemList=new Poem[]{poem1,poem2,poem3};
        Poem longestPoem = poemList [0];
        for (Poem poem: poemList){
            if (longestPoem.getStropheNumber() < poem.getStropheNumber()){
                longestPoem = poem;
            }
        }
        Author author = longestPoem.getCreator();
        System.out.println("author of the longestPoem: " + author.getSurname() );
    }
}
